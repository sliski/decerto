Simple application to perform operations on integer numbers.

1. Build

    - gradlew build
    
2. Deployment

To deploy location to application.yml config file must be provided in --spring.config.location parameter

java -jar decerto-0.0.1-SNAPSHOT.jar --spring.config.location=
    
3. Example usage

By default app runs on port 2020
  
Method GET: http://localhost:2020/api/randomOperation?min=10&max=15&operation=ADD
  
   
