package pl.decerto.values;

public class DecertoIntegerValue extends DecertoValue<Integer> {

  public DecertoIntegerValue(Integer value) {
    super(value);
  }

}
