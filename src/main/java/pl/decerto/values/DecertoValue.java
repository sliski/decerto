package pl.decerto.values;

public abstract class DecertoValue<T> {

  T value;

  protected DecertoValue(T value) {
    this.value = value;
  }

  public T getValue() {
    return value;
  }

}
