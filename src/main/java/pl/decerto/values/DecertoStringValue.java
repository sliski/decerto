package pl.decerto.values;

public class DecertoStringValue extends DecertoValue<String> {

  public DecertoStringValue(String value) {
    super(value);
  }

}
