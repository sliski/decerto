package pl.decerto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DecertoApp {

  public static void main(String[] args) {
    SpringApplication.run(DecertoApp.class, args);
  }

}
