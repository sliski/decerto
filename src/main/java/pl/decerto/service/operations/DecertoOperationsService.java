package pl.decerto.service.operations;

import java.util.List;
import org.springframework.stereotype.Service;
import pl.decerto.values.DecertoIntegerValue;
import pl.decerto.values.DecertoStringValue;
import pl.decerto.values.DecertoValue;

@Service
public class DecertoOperationsService {

  /**
   * Executes and returns result of given operation.
   *
   * @param values
   * @param operationDecerto operation to perform
   */
  public DecertoValue makeOperation(List<DecertoValue> values, OperationDecerto operationDecerto) {
    switch(operationDecerto) {
      case ADD:
        return add(values);
      case CONCAT:
        return concat(values);
      default:
        throw new IllegalArgumentException("Unknown operation");
    }
  }

  private DecertoValue add(List<DecertoValue> values) {
    Integer sum = 0;
    for(DecertoValue decertoValue: values) {
      if(decertoValue instanceof DecertoIntegerValue) {
        sum+=(((DecertoIntegerValue)decertoValue).getValue());
      }
      if(decertoValue instanceof DecertoStringValue) {
        try {
          sum += Integer.parseInt(((DecertoStringValue)decertoValue).getValue());
        } catch (NumberFormatException e) {
          throw new IllegalArgumentException("Given string" + ((DecertoStringValue)decertoValue).getValue() + " is not valid number");
        }
      }
    }
    return new DecertoIntegerValue(sum);
  }

  private DecertoValue concat(List<DecertoValue> values) {
    StringBuilder stringBuilder = new StringBuilder();
    for(DecertoValue decertoValue: values) {
      stringBuilder.append(decertoValue.getValue());
    }
    return new DecertoStringValue(stringBuilder.toString());
  }

}
