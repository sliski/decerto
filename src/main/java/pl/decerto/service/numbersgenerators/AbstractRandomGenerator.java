package pl.decerto.service.numbersgenerators;

import pl.decerto.values.DecertoValue;

public abstract class AbstractRandomGenerator<T> {

  /**
   * Validates input and return generated number.
   * @param a value a
   * @param b value b
   * @return generated number.
   */
  public DecertoValue getNumber(T a, T b) {
    try {
      validate(a, b);
    } catch(Exception e) {
      throw new IllegalArgumentException(e.getMessage());
    }
    return generate(a, b);
  }

  /**
   * Generates random number from given range.
   * @param a value
   * @param b value b
   * @return genrated number from given range.
   */
  protected abstract DecertoValue generate(T a, T b);

  /**
   * Validates if a and b are valid arguments
   * @param a value a
   * @param b value b
   */
  protected abstract void validate(T a, T b);

}
