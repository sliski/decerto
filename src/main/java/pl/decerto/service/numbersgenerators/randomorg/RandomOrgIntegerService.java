package pl.decerto.service.numbersgenerators.randomorg;

import java.util.Objects;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.decerto.service.numbersgenerators.AbstractRandomGenerator;
import pl.decerto.values.DecertoIntegerValue;
import pl.decerto.values.DecertoValue;

@Service
public class RandomOrgIntegerService extends AbstractRandomGenerator<Integer> {

  Logger logger = LoggerFactory.getLogger(RandomOrgIntegerService.class);

  @Value("${randomOrg.url}")
  public String serviceUrl;

  @Value("${randomOrg.apiKey}")
  public String apiKey;

  private final RestTemplate restTemplate;

  public RandomOrgIntegerService(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Override
  public DecertoValue generate(Integer min, Integer max) {
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);

    JSONObject body = prepareBody(min, max);

    HttpEntity<String> httpEntity = new HttpEntity<>(body.toString(), httpHeaders);

    try {
      ResponseEntity<String> exchange = restTemplate.exchange(
          serviceUrl, HttpMethod.POST, httpEntity, String.class);
      String response = exchange.getBody();
      return new DecertoIntegerValue(getNumberFromResponse(new JSONObject(Objects.requireNonNull(response))));
    } catch (Exception e) {
      logger.error("Error fetching number from random.org", e);
      throw e;
    }
  }

  @Override
  protected void validate(Integer a, Integer b) {
    if(a >= b) {
      throw new IllegalArgumentException("Min cannot be equal or larger than max");
    }
  }

  private JSONObject prepareBody(Integer min, Integer max) {
    JSONObject params = new JSONObject()
        .put("apiKey", apiKey)
        .put("n", 1)
        .put("min", min)
        .put("max", max)
        .put("replacement", true);

    return new JSONObject()
        .put("jsonrpc", "2.0")
        .put("method", "generateIntegers")
        .put("id", 42)
        .put("params", params);
  }

  private int getNumberFromResponse(JSONObject response) {
    return response.getJSONObject("result").getJSONObject("random").getJSONArray("data").getInt(0);
  }

}
