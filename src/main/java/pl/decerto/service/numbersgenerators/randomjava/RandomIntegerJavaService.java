package pl.decerto.service.numbersgenerators.randomjava;

import java.util.concurrent.ThreadLocalRandom;
import org.springframework.stereotype.Service;
import pl.decerto.service.numbersgenerators.AbstractRandomGenerator;
import pl.decerto.values.DecertoIntegerValue;
import pl.decerto.values.DecertoValue;

@Service
public class RandomIntegerJavaService extends AbstractRandomGenerator<Integer> {

  @Override
  protected DecertoValue generate(Integer min, Integer max) {
    return new DecertoIntegerValue(ThreadLocalRandom.current().nextInt(min, max + 1));
  }

  @Override
  protected void validate(Integer a, Integer b) {
    if(a >= b) {
      throw new IllegalArgumentException("Min cannot be equal or larger than max");
    }
  }
}
