package pl.decerto.service.numbersgenerators;

import org.springframework.stereotype.Service;
import pl.decerto.service.numbersgenerators.randomjava.RandomIntegerJavaService;
import pl.decerto.service.numbersgenerators.randomorg.RandomOrgIntegerService;
import pl.decerto.values.DecertoValue;

@Service
public class RandomIntegerValueFactory {

  private final RandomOrgIntegerService randomOrgIntegerService;
  private final RandomIntegerJavaService randomIntegerJavaService;

  public RandomIntegerValueFactory(
      RandomOrgIntegerService randomOrgIntegerService,
      RandomIntegerJavaService randomIntegerJavaService) {
    this.randomOrgIntegerService = randomOrgIntegerService;
    this.randomIntegerJavaService = randomIntegerJavaService;
  }

  public DecertoValue generateIntegerRandomNumber(RandomGenerators randomGenerator, Integer a, Integer b) {
    switch(randomGenerator) {
      case JAVA:
        return randomIntegerJavaService.getNumber(a, b);
      case RANDOM_ORG:
        return randomOrgIntegerService.getNumber(a, b);
      default:
        throw new UnsupportedOperationException("Unknown random generator");
    }
  }
}
