package pl.decerto.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.decerto.service.operations.OperationDecerto;
import pl.decerto.service.operations.DecertoOperationsService;
import pl.decerto.service.numbersgenerators.RandomIntegerValueFactory;
import pl.decerto.service.numbersgenerators.RandomGenerators;
import pl.decerto.values.DecertoValue;

@Controller
@RequestMapping("/api")
public class DecertoController {

  private final DecertoOperationsService decertoOperationsService;
  private final RandomIntegerValueFactory randomIntegerValueFactory;

  public DecertoController(DecertoOperationsService decertoOperationsService,
      RandomIntegerValueFactory randomIntegerValueFactory) {
    this.decertoOperationsService = decertoOperationsService;
    this.randomIntegerValueFactory = randomIntegerValueFactory;
  }

  @GetMapping(path = "randomOperation")
  public ResponseEntity<?> generateSumOfRandomNumbers(
      @RequestParam("min") int min,
      @RequestParam("max") int max,
      @RequestParam("operation")OperationDecerto operationDecerto) {


    List<DecertoValue> decertoValueList = new ArrayList<>();
    decertoValueList.add(randomIntegerValueFactory.generateIntegerRandomNumber(RandomGenerators.RANDOM_ORG, min, max));
    decertoValueList.add(randomIntegerValueFactory.generateIntegerRandomNumber(RandomGenerators.JAVA, min, max));

    try {
      return ResponseEntity.ok(decertoOperationsService.makeOperation(decertoValueList, operationDecerto).getValue());
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }

  }

}
